# Relatorio Git
Atividade 13 proposta na disciplina Intruducao ao Desenvolvimento Web - BCC/BSI no ano de 2023 ministrada pelo professor Wilson Massashiro.

## Criando o repositorio
Para criar o repositorio, primeiramente voce deve criar uma pasta referente ao repositorio git e entrar na pasta criada. Podem ser usados os comandos no Terminal Shell:
```
    mkdir rel-git
    cd rel-git
```

Depois de criada e acessada a pasta, deve-se inicializar o repositorio git com o seguinte comando:
```
    git init
```

Exemplo:

![exemplo criacao](criando-rep.png)

## Adicionando e modificando arquivos no repositorio
 
Uma vez iniciado o repositorio os arquivos e suas modificacoes serao monitoradas pela ferramenta do git.
Para checkar o status do repositorio, digite o comando:
```
    git status
```

![git status](git-status.png)

No exemplo acima, os arquivos em vermelho sao os arquivos que foram modificados ou adicionados em nosso diretorio.
O arquivo README.md eh este mesmo arquivo que voce esta lendo e o arquivo criando-rep.png eh a imagem utilizada na secao "Criando o repositorio" acima. O outro arquivo .swp eh um arquivo de controle criado pelo nano (editor usado para editar esse texto) para auxiliar na edicao do arquivo README.md.
Nos queremos adicionar ao repositorio todos os arquivos que nao sao .swp. Para isso criaremos um .gitignore.
O .gitignore eh um arquivo responsavel por mapear os arquivos que serao ignorados pelo git. Criaremos um com o seguinte conteudo, na raiz do projeto:
```
    *.swp
```
Desta forma o git ignorara todos os arquivos com essa extencao.
Daremos mais um ` git status ` para checar novamente os arquivos modificados e adicionados.

![git ignore status](gitignore-status.png)

Note que o arquivo .swp nao esta mais sendo mapeado apos a criacao do arquivo.

### Adicionando arquivos no staged
O staged eh um estagio do git no qual os arquivos selecionados para serem commitados no proximo commit ficam,
Uma vez que, por meio do git ignore, deixamos de mapear os arquivos indesejados, iremos adicionar todos os arquivos listados pelo git. Para isso utilizaremos o comando:
```
    git add .
```
No lugar do ` . ` podemos tambem colocar o nome do diretorio ou arquivo especificos que gostariamos de adicionar no staged.

Antes disso iremos executar mais um ` git status ` por via das duvidas XD.

![git status e git add](git-add.png)

## Commitando
Os commits sao os versionamentos do seu projeto. 
Commits devem ser feitos evitando erros no codigo versionado e com mensagens que te ajudem a rastrear o que foi feito caso o pior aconteca.

Antes de mais nada executaremos um ` git status ` como de costume:

![gitcommit-status](gitcommit-status.png)

Note que no primeiro status havia um arquivo em vermelho que logo adicionei ao staged.
Os arquivos em verde simbolizam os arquivos em staged que nao foram commitados ainda.

Para commitar os arquivos do staged, usaremos o comando:

```
    git commit -m "Titulo do commit"
```

![git commit](git-commit.png)

Agora commitarei essa secao que voce esta lendo tambem XD.

## Bonus: Repositorios remotos
Como um bonus criarei um repositorio no meu gitlab referente a este exemplo.
Para isso utilizarei o commando:
```
    git push --set-upstream https://gitlab.com/lucianoarendt/rel-git.git main
```

![criando repositorio remoto](setupstream.png)

![repositorio](repositorio.png)

Agora farei o commit referente a criacao da introducao dessa secao.

### Git push
![git push status](gitpush-status.png)

Quando realizamos o comando ` git commit `, versionamos apenas o repositorio local.
Para atualizar o repositorio remoto referente as mudancas feitas no local, executamos o commando:
```
    git push
```

![git push](git-push.png)

### Git pull
Em paralelo ao git push, o git pull serve para quando queremos atualizar o repositorio local em relacao ao repositorio remoto.
```
    git pull
```

## Obrigado pela atencao
Nao ha nada que nos diga:

![My job here is done](https://media.tenor.com/pagVxAkHfWAAAAAC/my-job-here-is-done-bye.gif)

Mais do que um:

![working tree clean](nothing-to-commit.png)
